import os
from random import *
import bpy
import bmesh
import string
import time
#num_dir_door = 4

#cleanup
try:
    bpy.ops.object.mode_set(mode='EDIT')
    for material in bpy.data.materials:
        material.user_clear()
        bpy.data.materials.remove(material)
        bpy.ops.object.mode_set(mode='OBJECT')
except RuntimeError:
    print("started empty")
bpy.ops.mesh.primitive_plane_add(size=2, enter_editmode=False, align='WORLD', location=(0, 0, 0), scale=(1, 1, 1))
bpy.ops.object.mode_set(mode='OBJECT')
bpy.ops.object.select_all(action='SELECT')
bpy.ops.object.delete(use_global=False)
#bpy.context.space_data.display_mode = 'ORPHAN_DATA'



room_size = 20
max_depth = 30

max_doors = 20
Z = 10
change = 2

#Use to override num_dir_door to ROOM_OVERRIDE
SIMPLE_ROOM_TEST = False
ROOM_OVERRIDE = 1

root_dir = "/home/david/myProjects/librem5_image_builder/tmp"
dirs_rendered = {}
paths_rendered = {}

#don't render files past limit_depth for limit_files_on
limit_files_on = ["/sys", "/var"]
limit_depth = 3


colors = {"/dev":    (255, 0, 0),
          "/etc":    (0, 255, 255),
          "/var":    (0, 0, 255),
          "/bin":    (0, 0, 160),
          "/boot":   (255, 90, 0),
          "/home":   ( 128, 0, 128 ),
          "/lib":    (255, 255, 0 ),
          "/media":  ( 0, 255, 0),
          "/mnt":    (128, 0, 0),
          "/opt":    (128, 128, 0),
          "/proc":   (0, 80, 0),
          "/root":   (255, 0, 255),
          "/run":    (165, 42, 42),
          "/sbin":   (255, 128, 64),
          "/srv":    (128, 128, 128),
          "/sys":    (106, 27, 154),
          "/tmp":    (0,0,0),
          "/usr":    (255, 255, 0),
          "/var":    (0, 0, 128)}
#setup root
def setup_room(offset=(0,0,0), root=False):
    bpy.ops.mesh.primitive_cube_add(size=room_size, location=offset, scale=(1, 1, .2))
    ob = bpy.context.object # get the active object for example
    mesh = ob.data
    #Enter edit mode to extrude
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.normals_make_consistent(inside=False)

    bm = bmesh.from_edit_mesh(mesh)
    for face in bm.faces:
        face.select = False
    bm.faces[1].select = True

def door_cut_select(bm, num_dir_door):
    for face in bm.faces:
        face.select = False
    total_faces = ((num_dir_door * 2) * num_dir_door * 2) + num_dir_door * 2
    for i in range(1, int(total_faces)):
        if i % 2 != 0:
            bm.faces[i *-1].select = True

def back_door_cut(bm, num_dir_door):
    for face in bm.faces:
        face.select = False
    #bm.faces[6].select = True
    #bm.faces[4].select = True
    bm.faces[3].select = True

    bpy.ops.transform.resize(value=(0.18, 0.2, .9), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
    bpy.ops.mesh.delete(type='FACE')

def exists(path):
    """Test whether a path exists.  Returns False for broken symbolic links"""
    try:
        os.stat(path)
    except OSError:
        return False
    return True

def cut_doors(num_dir_door, first=False):
    #cut out doors
    print("cutting")
    bpy.ops.mesh.subdivide(number_cuts=num_dir_door * 2)
    ob = bpy.context.object # get active mesh
    mesh = ob.data
    bm = bmesh.from_edit_mesh(mesh)
    bm.faces.ensure_lookup_table()
    
    back_door_cut(bm, num_dir_door)
    bm.faces.ensure_lookup_table()
    if num_dir_door == 0:
        return
    door_cut_select(bm, num_dir_door)
    
    bpy.ops.mesh.extrude_region_move(MESH_OT_extrude_region={"use_normal_flip":False, "use_dissolve_ortho_edges":False, "mirror":False}, TRANSFORM_OT_translate={"value":(0, num_dir_door*3, 0), "orient_type":'GLOBAL', "orient_matrix":((1, 0, 0), (0, 1, 0), (0, 0, 1)), "orient_matrix_type":'GLOBAL', "constraint_axis":(False, True, False), "mirror":False, "use_proportional_edit":False, "proportional_edit_falloff":'SMOOTH', "proportional_size":1, "use_proportional_connected":False, "use_proportional_projected":False, "snap":False, "snap_target":'CLOSEST', "snap_point":(0, 0, 0), "snap_align":False, "snap_normal":(0, 0, 0), "gpencil_strokes":False, "cursor_transform":False, "texture_space":False, "remove_on_cancel":False, "release_confirm":False, "use_accurate":False, "use_automerge_and_split":False})
    
    #bpy.ops.transform.resize(value=(2.67952, 1, 1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

    bpy.ops.transform.resize(value=(num_dir_door*2, num_dir_door*2, 1), orient_type='LOCAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='LOCAL', constraint_axis=(True, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

def set_color(name, color):
    if len(color) == 3:
        r, g, b = color
    elif len(color) == 4:
        r, g, b, a = color
    r = r/255
    g = g/255
    b = b/255
    color = (r,g,b, 1)
    ob = bpy.context.object # get active mesh
    m = ob.data
    activeObject = bpy.context.active_object
    mat = bpy.data.materials.new(name=name + str(color))
    mat.diffuse_color = color
    #set new material to variable
    activeObject.data.materials.append(mat) #add the material to the object
    print(dir(bpy.context.object.active_material))
        
def build_room(PWD, first=False, depth=0, offset=(0,0,0)):
    global Z
    global change
    global dirs_rendered
    global paths_rendered

    #setup pwd
    dirs_up_next = []
    try:
        files_and_dirs = os.listdir(PWD)
    except PermissionError:
        files_and_dirs = []
    for file_or_dir in files_and_dirs:
        file_or_dir = PWD + "/" + file_or_dir
        if os.path.isdir(file_or_dir):
            if not os.path.islink(file_or_dir):
                print(file_or_dir)
                dirs_up_next.append(file_or_dir)
    
    #dirs_up_next=["/home","/proc"]
    num_dir_door = len(dirs_up_next)
    if num_dir_door > max_doors:
        num_dir_door = max_doors
    if SIMPLE_ROOM_TEST:
        num_dir_door = ROOM_OVERRIDE
        dirs_up_next = ["fish"] * num_dir_door
    
    
    dirs_up_next.sort()
    print(dirs_up_next)

    if first:
        setup_room(root=True)
        dirs_rendered[root_dir] = (0,0,0)
    

    
    #set color
    color = None
    if PWD.startswith(root_dir):
        local_pwd = PWD.split(root_dir)[-1]
    else:
        local_pwd = PWD
    for colors_we_setup in colors:
        if local_pwd.startswith(colors_we_setup):
            color = colors[colors_we_setup]
    if color != None:
        set_color(local_pwd, color)
    

    
    #set name:
    obj = bpy.context.active_object
    obj.name = local_pwd
    obj.data.name = local_pwd
    
    #cut doors
    cut_doors(num_dir_door, first=first)
    paths_rendered[PWD] = [offset,dirs_up_next[:]]
    
    if num_dir_door == 0:
        return
    
    ob = bpy.context.object # get active mesh
    mesh = ob.data
    bm = bmesh.from_edit_mesh(mesh)
    bm.faces.ensure_lookup_table()
    active_faces = []
    for face in bm.faces:
        if face.select == True:
            active_faces.append(face)
    

    org_obj_list = {obj.name for obj in bpy.context.selected_objects}
    #s = bpy.context.selected_objects[0]
    #CUT off wall
    for dir_index in range(0, num_dir_door):
        
        for face in bm.faces:
            face.select = False

        for face_index in range(0, len(active_faces)):
            if face_index % num_dir_door == 0:
                active_faces[face_index-dir_index].select = True
        bpy.ops.mesh.edge_face_add()
        

        #move up
        rand = randint(1, num_dir_door)
        rand = rand - num_dir_door/2
        top_letter_div = 1
        for colors_we_setup in colors:
            if local_pwd.startswith(colors_we_setup):
                top_letter_div = list(colors).index(colors_we_setup)
                top_letter_div = top_letter_div/2 + 1

        amount_to_move = (change/top_letter_div) * num_dir_door*depth*(rand/2)
        print(amount_to_move)
        bpy.ops.transform.translate(value=(0, 0, amount_to_move), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
        
        #shrink
        #bpy.ops.transform.resize(value=(.2, .2, .2), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
        bpy.ops.transform.resize(value=(.2, .2, .90), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

        
        #cut
        bpy.ops.mesh.separate(type = 'SELECTED')
    #TODO remove this

    #build new rooms
    total_rooms = bpy.context.selected_objects[1:]
    if len(total_rooms) < num_dir_door:
        #If you get this, you're smarter than I am... Lol. 
        print(num_dir_door , total_rooms)
        #num_dir_door = len(total_rooms)
        total_rooms = bpy.context.selected_objects
    for dir_index in range(0, num_dir_door):
        #print("Yo")
        bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)
        #time.sleep(1)
        
        bpy.ops.object.editmode_toggle()
        for obj in bpy.context.selected_objects:
            obj.select_set(False)
        bpy.context.view_layer.objects.active = total_rooms[dir_index]
        #update
        bpy.ops.object.mode_set(mode='EDIT')
        ob = bpy.context.object # get active mesh
        mesh = ob.data
        bm = bmesh.from_edit_mesh(mesh)
        bm.faces.ensure_lookup_table()
        
        #get pos
        pos = ob.data.vertices[-1].co
        
        #delete face
        bpy.ops.mesh.select_all(action='SELECT')
        bpy.ops.mesh.delete(type='FACE')
        
        
        if num_dir_door == 1:
            pos = [pos[0]-room_size/28,pos[1]+room_size/4,pos[2]-room_size/70]
        else:
            pos = [pos[0]-room_size/20,pos[1]+room_size/4,pos[2]-room_size/25]

        #print(f"POS {pos}")
        
        #bpy.ops.mesh.primitive_cube_add(size=20, location=list(pos), scale=(1, 1, .2))
        #if last:
            #print(dir_index)
            #continue
        if depth < max_depth-1:
            dirs_rendered[dirs_up_next[dir_index]] = pos
            
            setup_room(offset=pos)
            build_room(dirs_up_next[dir_index], depth=depth+1, offset=pos)

def name_paths(paths_rendered):
    for pathname in paths_rendered:
        pos, dir_paths = paths_rendered[pathname]
        if len(dir_paths) <= 0:
            continue
        elif len(dir_paths) > max_doors:
            dir_paths = dir_paths[:max_doors]

        first = dir_paths[0]
        dir_paths = dir_paths[1:]
        dir_paths.reverse()
        tmp = [first]
        tmp.extend(dir_paths)
        dir_paths.extend([first])
        print(f"YoYo {pathname} {pos} {dir_paths}")
        #order.reverse()
        #dist_aprt = room_size/(len(dir_paths)-1)/2
        dist_aprt = room_size/(len((dir_paths)*2)+1)/2
        for dir_index in range(0, len(dir_paths)):
            dir_name = tmp[dir_index].split("/")[-1]
            dir_index = 1 + (dir_index *2)
            
            text_pos = [pos[0]+(dist_aprt*dir_index)-(room_size/4),pos[1]+(room_size/4.03),pos[2]]
            bpy.ops.object.text_add(enter_editmode=False, align='WORLD', location=text_pos, scale=(1, 1, 1))
            ob=bpy.context.object
            ob.data.body = dir_name
            #shrink text
            bpy.ops.transform.resize(value=(0.084535, 0.084535, 0.084535), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            #flip x 90
            bpy.ops.transform.rotate(value=1.5708, orient_axis='X', orient_type='LOCAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='LOCAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            #flip y 180
            bpy.ops.transform.rotate(value=3.14159, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

            set_color(dir_name, (1,1,1))

def name_folders(dirs_rendered):
    for dirname in dirs_rendered:
        pos = dirs_rendered[dirname]
        if dirname.startswith(root_dir):
            local_dirname = dirname.split(root_dir)[-1]
        else:
            local_dirname = dirname
        if local_dirname == "":
            local_dirname = "/"
        #print(local_dirname, pos)
        #setup text
        text_pos = [pos[0],pos[1],pos[2]]
        bpy.ops.object.text_add(enter_editmode=False, align='WORLD', location=text_pos, scale=(1, 1, 1))
        ob=bpy.context.object
        ob.data.body = local_dirname
        #shrink text
        bpy.ops.transform.resize(value=(0.34535, 0.34535, 0.34535), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
        
        #flip x 90
        bpy.ops.transform.rotate(value=1.5708, orient_axis='X', orient_type='LOCAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='LOCAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
        
        #flip y 180
        bpy.ops.transform.rotate(value=3.14159, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

        set_color(local_dirname, (1,1,1))
        #move up:
        #bpy.ops.transform.translate(value=(0, 0, ((height/2)+.01)*-1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
        
def add_files(dirs_rendered):
    for dirname in dirs_rendered:
        pos = dirs_rendered[dirname]
        #setup offset:
        pos = [pos[0],pos[1]-(room_size*.24),pos[2]+(room_size*.038)]
        if dirname.startswith(root_dir):
            local_dirname = dirname.split(root_dir)[-1]
        else:
            local_dirname = dirname
        files_to_show = []
        try:
            files_and_dirs = os.listdir(dirname)
        except Exception:
            files_and_dirs = []
        
        continue_loop = True
        for dirs_to_limit in limit_files_on:
            if local_dirname.startswith(dirs_to_limit):
                if len(local_dirname.split("/")) > limit_depth:
                    continue_loop = False
        if not continue_loop:
            print(f"Removed {local_dirname} from render")
            continue
        
        for file_or_dir in files_and_dirs:
            file_or_dir = dirname + "/" + file_or_dir
            if exists(file_or_dir) and not os.path.isdir(file_or_dir):
                if not os.path.islink(file_or_dir):
                    #print(file_or_dir)
                    files_to_show.append(file_or_dir)
        
        index = 0
        size_multipler = 0.4
        last_size = 0
        max_in_row = 42
        og_y = pos[1]
        for file_name in files_to_show:
            

            size = os.path.getsize(file_name)
            if size == 0:
                size = 1
            height = (size_multipler/20000000)*size
            height = 1*height
            if height > size_multipler:
                height = size_multipler
            if index % max_in_row == 0:
                pos[1] = og_y
                pos = [pos[0] + size_multipler/2,pos[1],pos[2]]
            else:
                pos = [pos[0],pos[1] + size_multipler/2,pos[2]]
            print(f"{file_name} pos:{index} size:{height}")
            #bpy.ops.mesh.primitive_plane_add(enter_editmode=False, align='WORLD', location=pos, scale=(size_multipler, size_multipler, size_multipler))
            
            #add cube file
            bpy.ops.mesh.primitive_cube_add(size=1, enter_editmode=False, align='WORLD', location=pos, scale=(size_multipler, size_multipler, height*2))
            
            obj = bpy.context.active_object
            obj.name = file_name
            obj.data.name = file_name
            
            
            #setup text
            text_pos = [pos[0],pos[1],pos[2]]
            bpy.ops.object.text_add(enter_editmode=False, align='WORLD', location=text_pos, scale=(1, 1, 1))
            ob=bpy.context.object
            ob.data.body = f"./{file_name.split('/')[-1]}"
            #shrink text
            bpy.ops.transform.resize(value=(0.0184535, 0.0184535, 0.0184535), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            #flip x 90
            bpy.ops.transform.rotate(value=1.5708, orient_axis='X', orient_type='LOCAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='LOCAL', constraint_axis=(True, False, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            #flip y 180
            bpy.ops.transform.rotate(value=3.14159, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)


            #move up:
            bpy.ops.transform.translate(value=(0, 0, ((height/2)+.01)*-1), orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(True, True, True), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)
            
            if file_name.startswith(root_dir):
                file_name = file_name.split(root_dir)[-1]
            
            #setup text color
            set_color(file_name, (1,1,1))

            bpy.ops.object.select_all(action='DESELECT')
            index = index + 1

#flip wold 180 over y
def flip_it():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.transform.rotate(value=3.14159, orient_axis='Y', orient_type='GLOBAL', orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), orient_matrix_type='GLOBAL', constraint_axis=(False, True, False), mirror=True, use_proportional_edit=False, proportional_edit_falloff='SMOOTH', proportional_size=1, use_proportional_connected=False, use_proportional_projected=False)

#dirs_rendered={'/home/david/myProjects/librem5_image_builder/tmp/boot': [29.060606002807617, 58.0, 0.0454545140266418]}
#dirs_rendered={'/home/david/myProjects/librem5_image_builder/tmp': (0, 0, 0), '/home/david/myProjects/librem5_image_builder/tmp/home': [-145.48484802246094, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/media': [145.4242401123047, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/opt': [126.03028869628906, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/dev': [106.6363525390625, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/sys': [87.24241638183594, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/run': [67.84848022460938, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/srv': [48.45454406738281, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/boot': [29.060606002807617, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/root': [9.666666984558105, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/usr': [-9.727272987365723, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/lost+found': [-29.1212100982666, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/mnt': [-48.51515197753906, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/etc': [-67.90908813476562, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/tmp': [-87.30302429199219, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/var': [-106.69696044921875, 58.0, 0.0454545140266418], '/home/david/myProjects/librem5_image_builder/tmp/proc': [-126.09089660644531, 58.0, 0.0454545140266418]}

build_room(root_dir, first=True)
print(dirs_rendered)
#setup files in dirs_rendered
bpy.ops.object.mode_set(mode='OBJECT')
add_files(dirs_rendered)
name_folders(dirs_rendered)
name_paths(paths_rendered)
flip_it()#Lol
#build_room("/1", ["1","2","3"], first=False)
#for door_that_needs_room_index in range(0, num_dir_door):
    
#bpy.ops.mesh.delete(type='FACE')
#bm.faces[-1].select = True
#bm.faces[-3].select = True
#bm.faces[-5].select = True
#bm.faces[-7].select = True
#bpy.ops.mesh.delete(type='FACE')


