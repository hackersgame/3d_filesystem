import bpy

for ob in bpy.data.objects:
    name = ob.name
    if name.startswith("/var") or name.startswith("/usr"):
        ob.hide_viewport = False
        print(name)
    else:
        ob.hide_viewport = True
